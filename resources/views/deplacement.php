<style>
th, td {
	padding: 10px;
}

th {
	text-align: right;
}
</style>

<h1 align="center">Projet laravel Deplacement</h1>
<form name="form" method="post">
	<table>
		<tbody>
			<tr>
				<th>Nom:</th>
				<td><input type="text" id="form_nom" name="form[nom]"
					required="required" /></td>
			</tr>
			<tr>
				<th>Prenom:</th>
				<td><input type="text" id="form_prenom" name="form[prenom]"
					required="required" /></td>
			</tr>
			<tr>
				<th>Email:</th>
				<td><input type="text" id="form_email" name="form[email]"
					required="required" /></td>
			</tr>
			<tr>
				<th>ville:</th>
				<td><select id="form_idville" name="form[idville]"><option
							value="Bizerte">Bizerte</option>
						<option value="Tunis">Tunis</option>
						<option value="Sousse">Sousse</option>
						<option value="Sfax">Sfax</option></select></td>
			</tr>
			<th>Date de depart:</th>
				<td><input type="date" id="form_datedepart" name="form[datedepart]"
					required="required" /></td>
			</tr>
			<tr>
			<th>Date de retour:</th>
				<td><input type="date" id="form_dateretour" name="form[dateretour]"
					required="required" /></td>
			</tr>
			<tr>
			<th>Heure de depart:</th>
				<td><input type="time" id="form_heuredepart" name="form[heuredepart]"
					required="required" /></td>
			</tr>
			<tr>
			<th>Heure de retour:</th>
				<td><input type="time" id="form_heureretour" name="form[heureretour]"
					required="required" /></td>
			</tr>
			<tr>
				<th>moyen de transport:</th>
				<td><div id="form_moyentransport">
						<input type="radio" id="form_moyentransport_0"
							name="form[moyentransport]" required="required" value="avion" /><label
							for="form_moyentransport_0" class="required">avion</label><input
							type="radio" id="form_moyentransport_1"
							name="form[moyentransport]" required="required" value="voiture" /><label
							for="form_moyentransport_1" class="required">voiture</label><input
							type="radio" id="form_moyentransport_2"
							name="form[moyentransport]" required="required" value="metro" /><label
							for="form_moyentransport_2" class="required">metro</label>
					</div></td>
			</tr>

			<tr>
				<th>Enregistrer :</th>
				<td><button type="submit" id="form_save" name="form[save]">enregistrer</button></td>
			</tr>
			<tr>
				<th>annuler :</th>
				<td><button type="submit" id="form_annuler" name="form[annuler]">annuler</button></td>
			</tr>
		</tbody>
	</table>
	<input type="hidden" id="form__token" name="form[_token]"
		value="EBN--wLBg5Q91Ru2A_VGPw37_quXbAzsPhEE97ruXlg" />
</form>


